import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ulab-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {
  private auxRating: number;

  @Input() rating: number;

  @Output() ratingChanged = new EventEmitter<number>();
  
  setRating() {
    this.ratingChanged.emit(this.auxRating);
  }
  
  restoreRating() {
    this.auxRating = this.rating;
  }

  constructor() { }

  ngOnInit() { 
    this.restoreRating();
  }
}
