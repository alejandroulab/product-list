import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PRODUCT_ROUTES } from './products.routes';
import { FormsModule } from '@angular/forms';
import { ProductFilterPipe } from './pipes/product-filter.pipe';
import { ProductOrderPipe } from './pipes/product-order.pipe';
import { ProductItemComponent } from './product-item/product-item.component';
import { StarRatingComponent } from '../rating/star-rating/star-rating.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductsService } from './services/products.service';
import { ProductDetailGuard } from './guards/product-detail.guard';
import { ProductDetailResolve } from './guards/product-detail-resolve.service';
import { RatingModule } from '../rating/rating.module';

@NgModule({
  declarations: [
    ProductFilterPipe,
    ProductOrderPipe,
    ProductItemComponent,
    StarRatingComponent,
    ProductListComponent,
    ProductDetailComponent,
    ProductEditComponent,
  ],
  imports: [
    CommonModule, 
    FormsModule, 
    RouterModule.forChild(PRODUCT_ROUTES), 
    RatingModule
  ],
  providers: [
    ProductsService, 
    ProductDetailGuard, 
    ProductDetailResolve
  ]
})
export class ProductsModule { }
