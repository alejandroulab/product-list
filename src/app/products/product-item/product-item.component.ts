import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { IProduct } from '../interfaces/i-product';

@Component({
  selector: 'ulab-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() product: IProduct;

  @Input() showImage: boolean;
  
  changeRating(rating: number) {
    this.productsService.changeRating(this.product.id, rating).subscribe(
      ok => this.product.rating = rating,
      error => console.error(error)
    );
  }
  
  constructor(private productsService: ProductsService) { }

  ngOnInit() {
  }

}
