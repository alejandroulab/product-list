import { Component, OnInit } from '@angular/core';
import { IProduct } from '../interfaces/i-product';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'ulab-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  filterSearch: string = '';
  showImage = true;
  title = 'Mi lista de productos';
  headers = {image:'Imagen', desc: 'Producto', price: 'Precio', avail: 'Disponible'};
  products: IProduct[] = [];

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsService.getProducts()
    .subscribe(
       prods => this.products = prods, // Success function
       error => console.error(error), // Error function (optional)
       () => console.log("Products loaded") // Finally function (optional)
    );
  }

  toggleImage () {
    this.showImage = !this.showImage;
  }
}
