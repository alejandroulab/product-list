import { Route } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductDetailGuard } from './guards/product-detail.guard';
import { ProductDetailResolve } from './guards/product-detail-resolve.service';
import { LeavePageGuard } from './guards/leave-page.guard';

export const PRODUCT_ROUTES: Route[] = [
  { 
    path: 'products', component: ProductListComponent 
  },
  // :id es un parámetro (id del producto)
  { 
    path: 'products/:id', 
    canActivate: [ProductDetailGuard], 
    component: ProductDetailComponent,
    resolve: {
      product: ProductDetailResolve
    } 
  },
  { 
	  path: 'products/edit/:id', 
    canActivate: [ProductDetailGuard], 
    canDeactivate: [LeavePageGuard], 
	  component: ProductEditComponent
  },
];