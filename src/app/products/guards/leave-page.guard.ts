import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentDeactivate } from '../interfaces/component-deactivate';

@Injectable({
  providedIn: 'root'
})
export class LeavePageGuard implements  CanDeactivate<ComponentDeactivate> {
  canDeactivate(
    component: ComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    // Recibimos el componente por parámetro → podemos acceder sus métodos
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
