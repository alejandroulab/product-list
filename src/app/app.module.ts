import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeComponent } from './welcome/welcome.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routes';
import { LeavePageGuard } from 'src/app/products/guards/leave-page.guard';
import { ProductsModule } from './products/products.module';
import { MenuModule } from './menu/menu.module';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    ProductsModule, 
    MenuModule, 
    BrowserModule, 
    FormsModule, 
    HttpClientModule, 
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    LeavePageGuard, 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
