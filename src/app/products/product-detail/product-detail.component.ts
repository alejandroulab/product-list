import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { IProduct } from 'src/app/products/interfaces/i-product';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'ulab-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  product: IProduct;
  
  constructor(private route: ActivatedRoute,
    private productService: ProductsService, 
    private router: Router) { }

  ngOnInit() {
    this.product = this.route.snapshot.data['product'];
/*    
    const id = this.route.snapshot.params['id']; // Recibimos parámetro
    this.productService.getProduct(id)
      .subscribe(
        p => this.product = p,
        error => console.error(error)
      );
*/
  }

  goBack() {
    this.router.navigate(['/products']);
  }

  edit() {
    this.router.navigate(['/products/edit', this.product.id]);
  }

  changeRating(rating: number) {
    this.productService.changeRating(this.product.id, rating).subscribe(
      ok => this.product.rating = rating,
      error => console.error(error)
    );
  }
}
